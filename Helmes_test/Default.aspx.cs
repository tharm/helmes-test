﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Load_Sectors();
        }
    }

    // Method for loading sector values from database to select box when the page loads
    // TODO:
    // 1) Change the DB to use multiple root scenario
    // 2) Allow user to select only items that don't have successors
    private void Load_Sectors()
    {
        using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["sectorsConnectionString"].ToString()))
        {
            try
            {
                string querySectors = @"SELECT 
                                            node.SectorId, 
                                            CONCAT(REPLICATE('&nbsp;&nbsp;&nbsp;&nbsp;', COUNT(parent.SectorName) - 1), node.SectorName) AS SectorName 
                                        FROM dbo.Sectors_ns AS node, dbo.Sectors_ns AS parent 
                                        WHERE node.Lft BETWEEN parent.Lft AND parent.Rgt 
                                        GROUP BY node.SectorId, node.SectorName, node.Lft 
                                        ORDER BY node.Lft";

                SqlDataAdapter sqlAdapter = new SqlDataAdapter(querySectors, sqlCon);
                DataSet ds = new DataSet();
                sqlAdapter.Fill(ds, "Sectors_ns");

                // Fill the select box automagically
                selSectors.DataSource = ds;
                selSectors.DataTextField = "SectorName";
                selSectors.DataValueField = "SectorId";
                selSectors.DataBind();

                // Iterate through the whole select list and modify where and if needed
                Regex rex = new Regex(" ");
                foreach (ListItem option in selSectors.Items)
                {
                    // Very unconvenient that we need to manually decode coded text to handle double coding
                    string temp = HttpUtility.HtmlDecode(rex.Replace(option.Text, " "));
                    option.Text = temp;
                }

                // Disable first item
                foreach (ListItem item in selSectors.Items)
                {
                    if (item.Value == "0")
                    {
                        item.Enabled = false;
                    }
                }

                selSectors.Items[0].Enabled = false;
            }
            catch (SqlException ex)
            {
                labelStatus.InnerText = "ERROR: Database connection issues: " + ex.Message;
            }
        }
    }

    protected void cbTermsAgreeVal_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = cbTermsAgree.Checked;
    }

    // Make insertions to database with submitted data
    // TODO: Check for duplicate SectorId insertions for ObjectName
    protected void submit_ServerClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            labelStatus.InnerText = "Successfully submitted: " + txtName.Value + ", sectors: ";
            foreach (ListItem item in selSectors.Items)
            {
                if (item.Selected)
                {
                    labelStatus.InnerText += item.Text + " ";
                }
            }

            // Store all input data to database(Name, Sectors)
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["sectorsConnectionString"].ToString()))
            {
                foreach (ListItem item in selSectors.Items)
                {
                    if (item.Selected)
                    {
                        SqlCommand sqlCmd = new SqlCommand();
                        sqlCmd.CommandText = @"INSERT INTO 
                                                   dbo.Object_data (ObjectName, SectorId) 
                                               VALUES 
                                                   ('" + txtName.Value + "', " + Int32.Parse(item.Value) + ")";
                        sqlCmd.Connection = sqlCon;

                        try
                        {
                            sqlCon.Open();

                            sqlCmd.ExecuteNonQuery();
                        }
                        catch (SqlException ex)
                        {
                            labelStatus.InnerText = "ERROR: Database connection issues: " + ex.Message;
                        }
                        finally
                        {
                            if (ConnectionState.Open == sqlCon.State)
                            {
                                sqlCon.Close();
                            }
                        }
                    }
                }
            }
        }
    }

    protected void load_ServerClick(object sender, EventArgs e)
    {
        labelStatus.InnerText = string.Empty;
        Load_ObjectData(txtName.Value);
    }


    // Extract sector data for specific objectName and activate them in select box
    private void Load_ObjectData(string objectName)
    {
        // First clear all fields user has activated
        selSectors.SelectedIndex = -1;

        using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["sectorsConnectionString"].ToString()))
        {
            List<string> sectorIds = new List<string>();

            if (0 != objectName.Length)
            {
                try
                {
                    sqlCon.Open();

                    SqlDataReader dr;

                    // Read the ObjectName rows from DB
                    SqlCommand sqlCmd = new SqlCommand(
                        @"SELECT 
                                SectorId
                            FROM dbo.Object_data
                            WHERE ObjectName = '" + objectName + "';",
                        sqlCon);

                    dr = sqlCmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            sectorIds.Add(dr.GetInt32(0).ToString());
                        }
                    }
                    else
                    {
                        labelStatus.InnerText = "Did not find entry with such name, please correct or add new entry";
                    }
                    dr.Close();
                }
                catch (SqlException ex)
                {
                    labelStatus.InnerText = "ERROR: Database connection issues: " + ex.Message;
                }
                finally
                {
                    if (ConnectionState.Open == sqlCon.State)
                    {
                        sqlCon.Close();
                    }
                }
            }

            // Lets activate found items in the select box
            foreach (ListItem option in selSectors.Items)
            {
                if (0 != sectorIds.Count())
                {
                    foreach (string id in sectorIds)
                    {
                        if (option.Value == id)
                        {
                            option.Selected = true;
                        }
                    }
                }
            }
        }
    }
}