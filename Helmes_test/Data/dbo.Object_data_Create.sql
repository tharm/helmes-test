﻿CREATE TABLE [dbo].[Object_data] (
    [OD_Id]      INT           IDENTITY (1, 1) NOT NULL,
    [ObjectName] NVARCHAR (50) NOT NULL,
    [SectorId]   INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([OD_Id] ASC),
    FOREIGN KEY ([SectorId]) REFERENCES [dbo].[Sectors_ns] ([SectorId])
);
