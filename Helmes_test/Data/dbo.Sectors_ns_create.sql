﻿CREATE TABLE [dbo].[Sectors_ns] (
    [S_Id]       INT           IDENTITY (1, 1) NOT NULL,
    [SectorName] NVARCHAR (50) NOT NULL,
    [SectorId]   INT           NOT NULL,
    [Lft]        INT           NOT NULL,
    [Rgt]        INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([S_Id] ASC),
    UNIQUE NONCLUSTERED ([SectorId] ASC)
);