﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>
<!DOCTYPE hmtl PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">

        <META http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title></title>
        <style type="text/css"></style>

        <script>
            function cbTermsAgreeVal_ClientValidate(sender, e)
            {
                e.IsValid = $("input:checkbox").prop('checked');
            }
        </script>

    </head>
    
    <body>
        <form id="sectors" runat="server">
            <table>
                <tr>
                    <td></td>
                    <td>
                        Please enter your name and either:<br />
                        a) Load existing records for modification <br />
                        b) Pick the Sectors you are currently involved in
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:RequiredFieldValidator runat=server 
                            ControlToValidate=txtName
                            Display="Dynamic"
                            ForeColor="#FF0000"
                            ValidationGroup='submitValGroup'
                            ErrorMessage="Name is required"
                            ID="txtNameVal"> *
                        </asp:RequiredFieldValidator>

                        <asp:RegularExpressionValidator runat=server
                            ControlToValidate="txtName"
                            ForeColor="#FF0000"
                            Display="Dynamic"
                            ValidationGroup="submitValGroup"
                            ValidationExpression="^[A-Za-z.'\-\p{L}\p{Zs}\p{Lu}\p{Ll}\']+.{1,50}$"
                            ErrorMessage="Name expression does not validate" 
                            ID="txtNameRegex"
                            Text="*">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td>
                        Name:

                        <br />

                        <input id="txtName" type="text" runat="server">
                    </td>
                </tr>
            
                <tr>
                    <td>
                        <asp:RequiredFieldValidator runat=server 
                            ControlToValidate=selSectors
                            Display="Static"
                            ForeColor="#FF0000"
                            ValidationGroup="submitValGroup"
                            ErrorMessage="Sector has to be chosen" 
                            ID="selSectorsVal"> *
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Sectors:

                        <br />

                        <select id="selSectors" multiple="true" size="10" runat="server"></select>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <asp:CustomValidator runat="server" 
                            EnableClientScript="true"
                            Display="Static"
                            ForeColor="#FF0000"
                            OnServerValidate="cbTermsAgreeVal_ServerValidate"
                            ClientValidationFunction="cbTermsAgreeVal_ClientValidate"
                            ValidationGroup="submitValGroup"
                            ErrorMessage="Agree to terms to submit" 
                            ID="cbTermsAgreeVal"> *
                        </asp:CustomValidator>
                    </td>
                    <td>
                        <input id="cbTermsAgree" type="checkbox" runat="server"> 
                            Agree to terms
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        <asp:ValidationSummary runat=server 
                            ValidationGroup="submitValGroup"
                            HeaderText="Please correct following errors to submit:" 
                            ForeColor="#FF0000">
                        </asp:ValidationSummary>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        <input id="submit" type="submit" Value="Save" onserverclick="submit_ServerClick" ValidationGroup="submitValGroup" runat="server"/>

                        <input id="load" type="submit" Value="Load" onserverclick="load_ServerClick" runat="server"/>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        <label id="labelStatus" runat="server" />
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>