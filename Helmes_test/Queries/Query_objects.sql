﻿SELECT ObjectName, SectorName, Object_data.SectorId
FROM 
  dbo.Object_data 
INNER JOIN 
  dbo.Sectors_ns 
ON Object_data.SectorId = Sectors_ns.SectorId
ORDER BY ObjectName;