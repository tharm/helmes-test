HELMES TEST
===========
Write .NET code to handle following tasks.

Tasks
-----
1. Correct all of the deficiencies in index.html
 
2. "Sectors" selectbox:
* Add all the entries from the "Sectors" selectbox to database
* Compose the "Sectors" selectbox using data from database
 
3. Perform the following activities after the "Save" button has been pressed:
* Validate all input data (all fields are mandatory)
* Store all input data to database (Name, Sectors, Agree to terms)
* Refill the form using stored data
* Allow the user to edit his/her own data during the session
 
Solution
--------
Really a very new area for me both ASP.NET and database wise. I would say for me the task was all about learning new technologies and moreover configuring different enviroments and tools.  
Please see code, database and improve subchapters in this docuemnt.   
  
Tools
-----
* Git for VCS
* MS Visual Studio Express 2015
* MS SQL Server 2014
* C# used as coding language
* Prefered HTML server controls over Web server controls (ASP.NET)

Code
----
* Default.aspx - client side implementation
* Default.aspx.cs - server side implementation

Database
--------
Two tables created for the task:
* Sectors_ns - nested set version of Sectors table, data pre-inserted
* Object_data - table to store runtime submitted data, relation to Sectors_ns through foreign key SectorId
Data insertion SQL commands included in Data/ folder.

Improve
----
Implementation-wise:
* Change the DB to use multiple root scenario?
* Allow user to select only items that don't have successors
* Check for duplicate SectorId insertions for ObjectName  
  
Overall improvements:
* Styling
* Testing - not really familiar with web testing
* Securing agains exploits
* DB structure improvement
* Query improvement
